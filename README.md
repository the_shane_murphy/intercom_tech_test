# Intercom Tech Test

A small program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).

### Getting Started

Clone the project and open with Intellij. To run the project ensure the configuration is set up correctly and then click Run

### Configurations

Main: run the main application
Tests: Configuration for running tests

### Running the tests

Change configuration to Tests and click on Run

### Output

Running the program will output a file to rrot directory - outputs.txt. This file and its output is included as part of the submission to the repo

### Author

**Shane Murphy**
