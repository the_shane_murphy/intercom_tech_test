package com.intercom.test.io;

import com.google.gson.Gson;
import com.intercom.test.model.Customer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CustomerFileReader {

  public List<Customer> readCustomersFromFile(File file) {
    List<Customer> customers = new ArrayList<>();
    BufferedReader reader;
    try {
      reader = new BufferedReader(new FileReader(file));
      String line = reader.readLine();
      while (line != null) {
        customers.add(convertToCustomer(line));
        line = reader.readLine();
      }
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return customers;
  }

  private Customer convertToCustomer(String line) {
    Gson gson = new Gson();
    return gson.fromJson(line, Customer.class);
  }

}
