package com.intercom.test.constants;

public class Constants {
  public static final String CUSTOMER_FILE_PATH = "assets/customers.txt";
  public static final String INVITATION_FILE_PATH = "output.txt";
  public static final double MAX_ALLOWABLE_DISTANCE_FROM_OFFICE_IN_KM = 100.0;
  public static final double OFFICE_LATITUDE = 53.339428;
  public static final double OFFICE_LONGITUDE = -6.257664;
  public static final int EARTH_RADIUS_IN_KM = 6371;
}
