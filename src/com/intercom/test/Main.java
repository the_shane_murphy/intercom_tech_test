package com.intercom.test;

import com.intercom.test.constants.Constants;

import java.io.File;

public class Main {
  public static void main(String[] args) {
    File file = new File(Constants.CUSTOMER_FILE_PATH);
    Inviter inviter = new Inviter(file);
    inviter.createInvitationList(inviter.getInvitees());
  }
}
