package com.intercom.test.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Customer implements Comparable<Customer> {
  private String name;
  @SerializedName("user_id")
  private int userId;
  private String latitude;
  private String longitude;

  public Customer(String name, int userId, String latitude, String longitude) {
    this.name = name;
    this.userId = userId;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public String getName() {
    return name;
  }

  public int getUserId() {
    return userId;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Customer customer = (Customer) o;
    return userId == customer.userId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId);
  }

  @Override
  public int compareTo(Customer comparableCustomer) {
    return Integer.compare(this.userId, comparableCustomer.userId);
  }
}
