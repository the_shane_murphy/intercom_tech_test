package com.intercom.test.utility;

import com.intercom.test.constants.Constants;

import static com.intercom.test.constants.Constants.OFFICE_LATITUDE;
import static com.intercom.test.constants.Constants.OFFICE_LONGITUDE;

public class DistanceFromOfficeCalculater {

  public double getDistance(String latitude, String longitude) {
    double latitudeRadians = degreesToRadians(Double.parseDouble(latitude));
    double longitudeRadians = degreesToRadians(Double.parseDouble(longitude));
    double centralAngle = calculateCentralAngle(latitudeRadians,longitudeRadians);
    return calculateDistance(centralAngle);
  }

  private double calculateCentralAngle(double latitude, double longitude) {
    double officeLatitude = degreesToRadians(OFFICE_LATITUDE);
    double officeLongitude = degreesToRadians(OFFICE_LONGITUDE);
    return radiansToDegrees(Math.acos((Math.sin(latitude) * Math.sin(officeLatitude))
            + (Math.cos(latitude) * Math.cos(officeLatitude) * Math.cos(
                    degreesToRadians(Math.abs(longitude - officeLongitude))))));
  }

  private double calculateDistance(double centralAngle) {
    return 2 * Math.PI * Constants.EARTH_RADIUS_IN_KM * (centralAngle / 360);
  }

  private double degreesToRadians(double latitude) {
    return latitude * Math.PI / 180;
  }

  private double radiansToDegrees(double radians) {
    return radians * (180 / Math.PI);
  }
}
