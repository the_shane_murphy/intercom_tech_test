package com.intercom.test;

import com.intercom.test.constants.Constants;
import com.intercom.test.io.CustomerFileReader;
import com.intercom.test.model.Customer;
import com.intercom.test.utility.DistanceFromOfficeCalculater;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Inviter {
  private File file;

  Inviter(File file) {
    this.file = file;
  }

  List<Customer> getInvitees() {
    List<Customer> customers = new CustomerFileReader().readCustomersFromFile(file);
    List<Customer> invitees = filterCustomersByDistance(customers);
    Collections.sort(invitees);
    return invitees;
  }

  void createInvitationList(List<Customer> invitees) {
    try {

      try (FileWriter writer = new FileWriter(Constants.INVITATION_FILE_PATH);
           BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
        for (Customer invitee : invitees) {
          bufferedWriter.write(invitee.getUserId() + " " + invitee.getName());
          bufferedWriter.newLine();
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private List<Customer> filterCustomersByDistance(List<Customer> customers) {
    List<Customer> invitees = new ArrayList<>();
    for (Customer customer : customers) {
      if (isInRange(customer)) {
        invitees.add(customer);
      }
    }
    return invitees;
  }

  private boolean isInRange(Customer customer) {
    double distanceFromOffice =
            new DistanceFromOfficeCalculater().getDistance(customer.getLatitude(), customer.getLongitude());
    return distanceFromOffice <= Constants.MAX_ALLOWABLE_DISTANCE_FROM_OFFICE_IN_KM;
  }
}
