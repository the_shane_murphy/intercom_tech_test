package com.intercom.test.io;

import com.intercom.test.model.Customer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

public class CustomerFileReaderTest {

  private File customersFile;

  @Before
  public void setUp() throws Exception {
    customersFile = new File("test/resources/customers.txt");
  }

  @Test
  public void readCustomersFromFile() {
    List<Customer> invitees = new CustomerFileReader().readCustomersFromFile(customersFile);
    Assert.assertFalse(invitees.isEmpty());
    Assert.assertEquals(4, invitees.size());
    Assert.assertTrue(invitees.get(0).getName().equalsIgnoreCase("Meath"));
    Assert.assertTrue(invitees.get(1).getName().equalsIgnoreCase("Australia"));
    Assert.assertTrue(invitees.get(2).getName().equalsIgnoreCase("Cork"));
    Assert.assertTrue(invitees.get(3).getName().equalsIgnoreCase("Dublin"));
  }
}