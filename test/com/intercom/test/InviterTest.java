package com.intercom.test;

import com.intercom.test.model.Customer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class InviterTest {
  private File customersFile;

  @Before
  public void setUp() throws Exception {
    customersFile = new File("test/resources/customers.txt");
  }

  @Test
  public void invite() {
    Inviter inviter = new Inviter(customersFile);
    List<Customer> invitees = inviter.getInvitees();
    Assert.assertFalse(invitees.isEmpty());
    Assert.assertEquals(2, invitees.size());
    Assert.assertTrue(invitees.get(0).getName().equalsIgnoreCase("Dublin"));
    Assert.assertTrue(invitees.get(1).getName().equalsIgnoreCase("Meath"));
  }
}